# Jobs Feed App


## Description

This project is done with React Js JavaScript framework. Project consist one page only. 
On The Home page is user dashboard where user can view it's own timeline with job feed. 
They are listed and paginated in 10 jobs per page. 
On the left end side there are filter, that filter and paginate the job feed in desired order. 

Google Maps are present, so when you click on the card on the text that says show on map it will Mark the actual job feed location.
 

## Installation
Download or clone code repository to your local machine


### Run the following command

### `npm install`
### `npm start`




