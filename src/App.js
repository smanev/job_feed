import 'bootstrap/dist/css/bootstrap.min.css';
import './scss/style.css'
import { Route, Switch } from 'react-router';
import Home from './components/pages/Home';
import Header from './components/ui/Header';


function App() {
  return (
    <div>
      <Header />
      <Switch>
        <Route exact path='/' component={Home} />
      </Switch>
    </div>
  );
}

export default App;
