const getJobs = () => {
    const jobs = [
        {
          job_title:
            "Physician / Geriatrics / Maine / Permanent / Geriatrics Job\nWeb Mktg Technology Specialist",
          organization_name: "The Fence Company",
          location_coordinates: ["43.919250488", "-69.967681885"],
        },
        {
          job_title: "CDL B Truck Driver - $18.00/Hour - Home Daily!",
          organization_name: "Hogan",
          location_coordinates: ["39.7684021", "-86.158065796"],
        },
        {
          job_title: "Sales Team Member JET FOOD STORE #64",
          organization_name: "Jet Food Stores, Inc",
          location_coordinates: ["33.215702057", "-82.468185425"],
        },
        {
          job_title:
            "Overnight Grocery Team Member (Full Time) - 115 Prospect St. Cambridge, MA",
          organization_name: "Whole Foods Market, Inc.",
          location_coordinates: ["42.373615265", "-71.109733582"],
        },
        {
          job_title: "Major Gift Officer",
          organization_name: "The Everglades Foundation Inc",
          location_coordinates: ["25.761680603", "-80.19178772"],
        },
        {
          job_title: "Account Executive Market Research",
          location_coordinates: ["38.90719223", "-77.036872864"],
        },
        {
          job_title: "Essential Worker - Warehouse Order Filler (Edison)",
          organization_name: "Amazon.com, Inc.",
          location_coordinates: ["40.788433075", "-74.133201599"],
        },
        {
          job_title:
            "CDL Truck Driver - Average $1,400-$1,500/Week - Excellent Benefits",
          organization_name: "Kehe - New Brighton Mn",
          location_coordinates: ["45.306907654", "-92.362136841"],
        },
        {
          job_title: "Assistant Activity Coordinator",
          organization_name: "Integrity Healthcare of Clarksville, LLC",
          location_coordinates: ["36.529769897", "-87.359451294"],
        },
        {
          job_title: "COOK",
          organization_name: "Bowlero Limited",
          location_coordinates: ["39.2903862", "-76.612190247"],
        },
        {
          job_title: "RN - Home Care in Tillamook",
          organization_name: "Premier Medical Staffing Inc",
          location_coordinates: ["45.456214905", "-123.84401703"],
        },
        {
          job_title: "Finance Manager, Retail Central Finance",
          organization_name: "Expedia Inc.",
          location_coordinates: ["42.365921021", "-73.116210938"],
        },
        {
          job_title: "Patient Financial Service Rep",
          organization_name: "Frederick Memorial Healthcare System",
          location_coordinates: ["39.414268494", "-77.41053772"],
        },
        {
          job_title:
            "Senior Product Manager, Payer Product (Health Plan Design/Claims Adjudication SME)",
          organization_name: "Olive",
          location_coordinates: ["47.613132477", "-122.33705139"],
        },
        {
          job_title:
            "Licensed Medicare Insurance Agent - Work from Home, Great Full-Time Sales Opportunity",
          organization_name: "ASSURANCE IQ, INC",
          location_coordinates: ["40.232311249", "-76.88469696"],
        },
        {
          job_title: "Customer Service & Sales Associate - HIRING BONUS",
          organization_name: "Hertz Global Holdings, Inc.",
          location_coordinates: ["39.529632568", "-119.81380463"],
        },
        {
          job_title: "Class A CDL Solo Truck Driver - Dedicated Lanes",
          organization_name: "Hendrickson Truck Parts Inc",
          location_coordinates: ["40.609668732", "-111.93910217"],
        },
        {
          job_title: "Software Engineer in Test - Automation Engineer",
          organization_name: "Aiserm Inc",
          location_coordinates: ["37.431026459", "-122.10122681"],
        },
        {
          job_title: "Recruitment Senior Manager - Military Veterans",
          organization_name: "Accenture LLC",
          location_coordinates: ["38.223880768", "-86.586265564"],
        },
        {
          job_title: "Motion Graphics Designer",
          location_coordinates: ["38.90719223", "-77.036872864"],
        },
        {
          job_title: "CDL Driver",
          organization_name: "Brenntag North America, Inc.",
          location_coordinates: ["42.479263306", "-71.152275085"],
        },
        {
          job_title: "Retail Inventory Associate - 4301",
          location_coordinates: ["46.877185822", "-96.789802551"],
        },
        {
          job_title: "School Bus Driver",
          location_coordinates: ["35.819789886", "-88.915893555"],
        },
        {
          job_title: "Analyst, Investments",
          organization_name: "Jones Lange and Lasalle",
          location_coordinates: ["37.774929047", "-122.41941833"],
        },
        {
          job_title:
            "Delivery Associate DCL4 Cleveland, OH (Starting pay $16.50/hr+). Job in Munroe Falls Gr8Jobs",
          organization_name: "Amazon.com, Inc.",
          location_coordinates: ["41.144500732", "-81.439834595"],
        },
        {
          job_title: "Part-time Associate Attorney - Insurance Defense",
          location_coordinates: ["34.075374603", "-84.294090271"],
        },
        {
          job_title: "Oracle Apps Technical",
          organization_name: "Wipro Limited",
          location_coordinates: ["36.153980255", "-95.992774963"],
        },
        {
          job_title: "Lease Analyst",
          organization_name: "Amazon.com, Inc.",
          location_coordinates: ["34.048336029", "-118.25508118"],
        },
        {
          job_title: "Urgently Hiring Student Truck Drivers - No Experience",
          organization_name: "C.R. England, Inc.",
          location_coordinates: ["43.998580933", "-96.312812805"],
        },
        {
          job_title:
            "Lift Truck Jobs: Hiring Immediately Full Time / Part Time - $18-$36/Hr",
          organization_name: "Warehouse Jobs",
          location_coordinates: ["37.05827713", "-120.84991455"],
        },
        {
          job_title: "(USA) Staff Pharmacist",
          organization_name: "Wal-Mart Stores, Inc.",
          location_coordinates: ["41.210651398", "-74.01763916"],
        },
        {
          job_title: "Merchandise Associate",
          organization_name: "TJX Companies, Inc.",
          location_coordinates: ["33.898159027", "-84.283256531"],
        },
        {
          job_title: "Dishwasher and Food Prep",
          organization_name: "Merriland Farm",
          location_coordinates: ["43.322231293", "-70.58052063"],
        },
        {
          job_title: "Design Email Signature",
          organization_name: "Upwork Inc.",
          location_coordinates: ["41.519924164", "-81.515213013"],
        },
        {
          job_title: "Cashier & Front End Services",
          organization_name: "Wal-Mart Stores, Inc.",
          location_coordinates: ["61.218055725", "-149.90028381"],
        },
        {
          job_title: "Assistant General Manager-Ballpark Operations",
          organization_name: "Firehouse Subs",
          location_coordinates: ["33.66198349", "-112.34127045"],
        },
        {
          job_title: "Local Yard dog driver- CDL Class A Yard Hostler/Driver",
          organization_name: "Brown Logistics Services, Inc.",
          location_coordinates: ["32.294773102", "-81.235359192"],
        },
        {
          job_title:
            "Lancome - - Account Coordinator - ROOSEVELT FIELD - GARDEN CITY, NY",
          organization_name: "Loreal",
          location_coordinates: ["40.726768494", "-73.634292603"],
        },
        {
          job_title: "Lyft Driver (Part-Time/Full-Time)",
          organization_name: "Lyft Inc",
          location_coordinates: ["39.66204071", "-75.566818237"],
        },
        {
          job_title: "Senior Mortgage Underwriter",
          organization_name: "Cornerstone Home Lending, Inc.",
          location_coordinates: ["36.153980255", "-95.992774963"],
        },
        {
          job_title: "Hiring CDL-A for Local - Home Daily and OTR - Home Weekly",
          organization_name: "Barney Trucking, Incorporated",
          location_coordinates: ["40.299404144", "-109.9887619"],
        },
        {
          job_title: "Pressure Washing Technicians/Supervisors",
          location_coordinates: ["29.785776138", "-95.824508667"],
        },
        {
          job_title: "Information Security Analyst - 100% Telework!",
          organization_name: "General Dynamics Information Technology",
          location_coordinates: ["39.268112183", "-84.413276672"],
        },
        {
          job_title: "Life Insurance Agent $100K+ Year One",
          organization_name: "Texas Professional Insurance Agents",
          location_coordinates: ["38.90719223", "-77.036872864"],
        },
        {
          job_title:
            "Hiring CDL Truck Drivers - Great Home Time - Average $813/Week",
          organization_name: "C.R. England, Inc.",
          location_coordinates: ["30.306098938", "-97.952377319"],
        },
        {
          job_title: "Local CDL-A Owner Operator Truck Driver",
          organization_name: "Florida Rock & Tank Lines, Inc.",
          location_coordinates: ["32.46402359", "-86.459693909"],
        },
        {
          job_title: "Data Entry Specialist\nWeb Mktg Technology Specialist",
          organization_name: "Spear Education, LLC",
          location_coordinates: ["33.646251678", "-111.92355347"],
        },
        {
          job_title: "Office Systems Administrator Position in Telecoms",
          organization_name: "Corporate Support Services, Inc",
          location_coordinates: ["39.869762421", "-75.127380371"],
        },
        {
          job_title: "Financial Manager, Senior",
          organization_name: "Hx5, LLC",
          location_coordinates: ["35.467559814", "-97.516426086"],
        },
        {
          job_title: "Major Accounts Sales Representative",
          organization_name: "Automatic Data Processing, Inc.",
          location_coordinates: ["35.467559814", "-97.516426086"],
        },
        {
          job_title: "CDL-A Driving Jobs - 1 Yr OTR EXP Req'd - Dedicated Lanes",
          organization_name: "R.E. Garrison Trucking, Inc.",
          location_coordinates: ["40.463615417", "-74.532562256"],
        },
        {
          job_title: "CEQA Associate/Technical Writer",
          organization_name: "Raney Planning and Management, Inc",
          location_coordinates: ["38.581573486", "-121.49440002"],
        },
        {
          job_title: "FOH - Great Service Representative - Cashier",
          organization_name: "Leisure Recruiter",
          location_coordinates: ["41.666156769", "-81.339553833"],
        },
        {
          job_title: "FT Laundry Aide HCC\nWeb Mktg Technology Specialist",
          organization_name: "Hopebridge",
          location_coordinates: ["33.748996735", "-84.387985229"],
        },
        {
          job_title: "Sr Sales Representative - Virtual Sales Career Fair",
          organization_name: "The Collegiate Entrepreneurs Organization",
          location_coordinates: ["39.647766113", "-104.98776245"],
        },
        {
          job_title: "Full-Time Assistant Store Manager",
          organization_name: "ALDI Inc.",
          location_coordinates: ["42.062992096", "-88.122718811"],
        },
        {
          job_title: "Administrative Assistant",
          organization_name: "Grace Healthcare Services",
          location_coordinates: ["40.518714905", "-74.412094116"],
        },
        {
          job_title: "LPN - Canton, OH Float $500 SIGN ON BONUS",
          organization_name: "CVS Health",
          location_coordinates: ["40.875892639", "-81.402336121"],
        },
        {
          job_title:
            "CDL Need Truck Driver Now, 06/01/2021, Top Pay - Great Benefits",
          organization_name: "U. S. Xpress, Inc.",
          location_coordinates: ["38.930152893", "-76.859695435"],
        },
        {
          job_title:
            "Part-Time Associate Banker (Market Expansion) - Towson Station - Greater Baltimore",
          organization_name: "JPMorgan Chase & Co.",
          location_coordinates: ["39.401496887", "-76.601913452"],
        },
        {
          job_title: "Medical Office Administrator",
          location_coordinates: ["34.210388184", "-77.886810303"],
        },
        {
          job_title: "RN Care Manager",
          organization_name: "Community, Inc.",
          location_coordinates: ["42.360080719", "-71.058883667"],
        },
        {
          job_title: "Skilled Electricians Wanted",
          location_coordinates: ["39.739234924", "-104.99024963"],
        },
        {
          job_title:
            "At Your Service Center Associate, Dulles Town Center - Variable Hours",
          organization_name: "Macy's Inc.",
          location_coordinates: ["39.006698608", "-77.429130554"],
        },
        {
          job_title: "Special Education Teacher (ESY)",
          organization_name: "Therapy Travels",
          location_coordinates: ["42.776203156", "-71.077278137"],
        },
        {
          job_title: "OR",
          organization_name: "Titan Medical Inc",
          location_coordinates: ["32.500701904", "-94.740486145"],
        },
        {
          job_title: "Nanny Needed For Sweet 7-month Old In Salt Lake City, Utah",
          location_coordinates: ["40.760780334", "-111.89104462"],
        },
        {
          job_title: "UU Student - Other",
          organization_name: "UNIVERSITY OF UTAH",
          location_coordinates: ["40.762042999", "-111.8419342"],
        },
        {
          job_title:
            "NO EXPERIENCE NECESSARY Electrical Apprenticeship Opening Fri. 5/21",
          organization_name: "Electric",
          location_coordinates: ["39.739234924", "-104.99024963"],
        },
        {
          job_title: "Food & Grocery",
          organization_name: "Wal-Mart Stores, Inc.",
          location_coordinates: ["47.505283356", "-111.30077362"],
        },
        {
          job_title: "Pega Senior System Architect",
          location_coordinates: ["32.776664734", "-96.796989441"],
        },
        {
          job_title: "COS - Line Service Technician 2nd Shift",
          organization_name: "Kearney Neeb & Company Inc",
          location_coordinates: ["38.833881378", "-104.82136536"],
        },
        {
          job_title: "Work in Retail? Earn Extra Cash Delivering Groceries",
          organization_name: "Shipt, Inc.",
          location_coordinates: ["40.336776733", "-74.047080994"],
        },
        {
          job_title: "Auto Care Center",
          organization_name: "Wal-Mart Stores, Inc.",
          location_coordinates: ["38.765113831", "-76.8983078"],
        },
        {
          job_title: "Senior Associate, Data Stewardship",
          organization_name: "Santander Holdings Usa, Inc.",
          location_coordinates: ["32.93429184", "-97.078063965"],
        },
        {
          job_title: "CUSTOMER SERVICE-PACE-AA1015",
          organization_name: "High Desert Oil Inc",
          location_coordinates: ["40.167205811", "-105.10192871"],
        },
        {
          job_title: "Credit and Wholesale Banking Summer Analyst Program",
          organization_name: "Bank of America",
          location_coordinates: ["36.216796875", "-81.674552917"],
        },
        {
          job_title: "Lead Network Engineer - Military Veterans",
          organization_name: "Ntt Data, Inc.",
          location_coordinates: ["39.083995819", "-77.152755737"],
        },
        {
          job_title: "Machine Operator II - 1st Shift",
          organization_name: "Generac",
          location_coordinates: ["44.202209473", "-88.446495056"],
        },
        {
          job_title:
            "Physician - Psychiatry - Geriatric - Physician (Psychiatry) â€ Franciscan Physician Network â€ Laf",
          organization_name: "Franciscan Physician Network",
          location_coordinates: ["40.487262726", "-85.61302948"],
        },
        {
          job_title: "Telehealth Coordinator",
          organization_name: "Advent Health",
          location_coordinates: ["37.153701782", "-83.761863708"],
        },
        {
          job_title: "Tenure Track Assistant or Associate Professor; Oral Cancer",
          organization_name: "University of Minnesota",
          location_coordinates: ["44.977752686", "-93.265007019"],
        },
        {
          job_title: "Dispatcher",
          organization_name: "United Rentals",
          location_coordinates: ["38.821464539", "-121.19299316"],
        },
        {
          job_title: "Associate Director, Commercial Analytics",
          organization_name: "Lantheus Holdings, Inc.",
          location_coordinates: ["42.558422089", "-71.268943787"],
        },
        {
          job_title: "Auto Technician Wanted",
          location_coordinates: ["32.866165161", "-96.721260071"],
        },
        {
          job_title: "Physical Therapist",
          organization_name: "Surge Talent Solutions",
          location_coordinates: ["35.915370941", "-94.969955444"],
        },
        {
          job_title: "Systems Administrator (O365 / Azure)",
          organization_name: "Objective Paradigm Corporation",
          location_coordinates: ["41.878112793", "-87.629798889"],
        },
        {
          job_title: "Guest Service Associate/Cashier",
          organization_name: "Global Partners LP",
          location_coordinates: ["44.450790405", "-73.206802368"],
        },
        {
          job_title: "Client Success Manager I- Time - Military Veterans",
          organization_name: "Automatic Data Processing, Inc.",
          location_coordinates: ["40.865287781", "-74.417388916"],
        },
        {
          job_title: "Server / Tasting Room Associate",
          organization_name: "Presqu'ile Winery",
          location_coordinates: ["34.953033447", "-120.43572235"],
        },
        {
          job_title: "Server",
          organization_name: "Outback Steakhouse of Florida, Inc.",
          location_coordinates: ["38.969554901", "-77.386100769"],
        },
        {
          job_title: "Benefits Coordinator - Denver Based/Non-Remote",
          organization_name: "Growgeneration Corp.",
          location_coordinates: ["39.739234924", "-104.99024963"],
        },
        {
          job_title: "Loan Officer- Consumer Loan Underwriter",
          organization_name: "ABNB Federal Credit Union",
          location_coordinates: ["36.76820755", "-76.287490845"],
        },
        {
          job_title: "Software Engineer, Intern/Co-op",
          organization_name: "Facebook Inc.",
          location_coordinates: ["40.760780334", "-111.89104462"],
        },
        {
          job_title: "Construction Laborer",
          organization_name: "Perma-Bilt Industries, Inc.",
          location_coordinates: ["47.052879333", "-122.29428101"],
        },
        {
          job_title: "Manager, Evidence & Discovery Management",
          organization_name: "Kpmg LLP",
          location_coordinates: ["41.878112793", "-87.629798889"],
        },
        {
          job_title: "IT HelpDesk Analyst II (Commack NY/Cleveland OH)",
          location_coordinates: ["40.842876434", "-73.292892456"],
        },
        {
          job_title: "Director - Pathways Learning Academy of Boulder",
          organization_name: "Jobleaders, Inc.",
          location_coordinates: ["40.014984131", "-105.27054596"],
        },
        {
          job_title: "Relationship Banker - Bell / Thompson Peak Financial Center",
          organization_name: "Bank of America",
          location_coordinates: ["33.613204956", "-111.92642975"],
        },
        {
          job_title: "Sales Program Associate",
          organization_name: "News America Marketing",
          location_coordinates: ["40.735656738", "-74.172363281"],
        },
      ];

      return jobs
}

export default getJobs
