import React from "react";
import getJobs from "../api/getJobs";
import Jobs from "../ui/Jobs";
import { Map, Marker, GoogleApiWrapper } from "google-maps-react";
import { Spinner } from "react-bootstrap";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      jobs: [],
      tempJobs: [],
      search: "",
      by: "",
      direction: "asc",
      loading: true,
      lat: "",
      lng: "",
    };
  }

  componentDidMount = async () => {
    let res = await getJobs();
    this.setState({
      jobs: res,
      loading: false,
      tempJobs: res,
    });
  };

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = event.target.name;
    this.setState(
      {
        [name]: value,
      },
      this.filterJobs
    );
  };

  filterJobs = () => {
    let temp = [...this.state.jobs];
    if (this.state.search !== "") {
      temp = temp.filter((job, index) => {
        if (job.job_title && job.organization_name) {
          let res =
            job.job_title.toLowerCase().includes(this.state.search) ||
            job.organization_name.toLowerCase().includes(this.state.search);
          return res;
        }
      });
    }

    if (this.state.direction !== "") {
      temp = temp.sort((a, b) => {
        const sortdir = this.state.direction === "asc" ? 1 : -1;
        if (
          this.state.by !== "" &&
          this.state.by === "job_title" &&
          a.job_title
        ) {
          return sortdir * a.job_title.localeCompare(b.job_title);
        } else if (
          this.state.by !== "" &&
          this.state.by === "organization_name" &&
          a.organization_name
        ) {
          return (
            sortdir * a.organization_name.localeCompare(b.organization_name)
          );
        } else {
          return;
        }
      });
    }
    this.setState({ tempJobs: temp });
  };

  removeJob = (job_title) => {
    let temp = [...this.state.tempJobs];
    temp = temp.filter((job) => job.job_title !== job_title);
    return this.setState({ tempJobs: temp });
  };

  changeLocation = (location_coordinates) => {
    this.setState({
      lat: location_coordinates[0],
      lng: location_coordinates[1],
    });
  };
  render() {
    if (this.state.loading) {
      return (
        <div className='d-flex min-h-100 justify-content-center p-5 align-items-center'>
          <Spinner animation="border" role="status">
            <span className="sr-only">Loading...</span>
          </Spinner>
        </div>
      );
    }
    const containerStyle = {
      position:'relative',
      width: "100%",
      height: "400px",
    };

    return (
      <div className="container">
        <div className="row my-5 flex-column flex-md-row">
          <div className="col-12 col-md-4">
            <div className='container'>
              <div className="row">
                <div className="col-12">
                  <h3>Filters</h3>
                  <div className="form-group">
                    <label>Search By Title or Organization Name</label>
                    <input
                      type="text"
                      id="search"
                      name="search"
                      onChange={this.handleChange}
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label>Search By Title or Organization Name</label>
                    <select
                      type="text"
                      name="by"
                      className="form-control"
                      onChange={this.handleChange}
                    >
                      <option value="">Sort by</option>
                      <option value="job_title">Job Title</option>
                      <option value="organization_name">Organization Name</option>
                    </select>
                  </div>
                  <div className="form-group">
                    <label>Search By Title or Organization Name</label>
                    <select
                      type="text"
                      name="direction"
                      className="form-control"
                      onChange={this.handleChange}
                    >
                      <option value="">Select</option>
                      <option value="asc">ASC</option>
                      <option value="desc">DESC</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 p-2">
                  <Map
                    className='position-relative'
                    style={containerStyle}
                    google={this.props.google}
                    zoom={14}
                    center={{
                      lat: this.state.lat,
                      lng: this.state.lng,
                    }}
                  >
                    <Marker
                      position={{ lat: this.state.lat, lng: this.state.lng }}
                    />
                  </Map>
                </div>
              </div>
            </div>
          </div>
          <div className="col-12 col-md-8">
            <h3>My Jobs Feed</h3>
            <Jobs
              jobs={this.state.tempJobs}
              removeJob={this.removeJob}
              changeLocation={this.changeLocation}
            />
          </div>
        </div>
      </div>
    );
  }
}

// export default
// export default Home;

export default GoogleApiWrapper({
  apiKey: process.env.MAPS_API,
})(Home);
