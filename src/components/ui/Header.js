import React from 'react'
import { Navbar } from 'react-bootstrap'

const Header = () => {
    return (
        <>
            <Navbar bg="light">
                <Navbar.Brand href="#home">User Dashboard</Navbar.Brand>
            </Navbar>
        </>
    )
}

export default Header
