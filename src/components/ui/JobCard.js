import React from 'react'
import { MdDeleteSweep } from 'react-icons/md'
import { FaLocationArrow } from 'react-icons/fa'

const JobCard = ({job_title, organization_name, location_coordinates, removeJob, changeLocation}) => {
    return (
        <div className="card my-3">
            <div className="card-header font-weight-bold">{organization_name}</div>
            <div className="card-body">
                <div className="card-text"><small> {job_title}</small></div>
            </div>
            <div className="card-footer">
                <div className="row">
                    <div className="col">
                        <button className='btn btn-link' onClick={() => changeLocation(location_coordinates)}>
                            <span className='mr-3'>
                                <FaLocationArrow className='mr-2 text-primary' />
                                show on map
                            </span>
                        </button>
                    </div>
                    <div className="col d-flex justify-content-end">
                        <button className='btn' 
                        onClick={() => { if (window.confirm('Are you sure you wish to remove this listing?')) removeJob(job_title) }}>
                            <MdDeleteSweep className='text-danger' />
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default JobCard
