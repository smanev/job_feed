import React, { useState } from 'react'
import { Spinner } from 'react-bootstrap'
import JobCard from './JobCard'
import Paginate from './Paginate'

const Jobs = ({jobs, removeJob, changeLocation}) => {
    const [postsPerPage] = useState(10)
    const [currentPage, setcurrentPage] = useState(1)
    const indexOfLastJob = currentPage * postsPerPage
    const indexOfFirstJob = indexOfLastJob - postsPerPage
    let currentJobs = jobs.slice(indexOfFirstJob, indexOfLastJob)
    if (!jobs) {
        return (
            <div className='d-flex h-100 justify-content-center p-5 align-items-center'>
                <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>
                Loading...
            </div>
        )
    }
    const paginate = (e, pageNumber) => {
        setcurrentPage(pageNumber)
        let links = document.getElementsByClassName('page-link')
        links = [...links]
        links.forEach(link => {
            if (link.classList.contains('active')) {
                link.classList.remove('active')
            }
        });
        let actualLink = e
        actualLink.classList.add('active')
        window.scrollTo({top: 0, behavior: 'smooth'})
    }
    return (
        <div>
            {currentJobs.length < 1 ? <div>No jobs listed</div> :
            currentJobs.map(job => {
                return <JobCard 
                    key={job.job_title}
                    job_title={job.job_title}
                    organization_name={job.organization_name}
                    location_coordinates={job.location_coordinates}
                    removeJob={removeJob}
                    changeLocation={changeLocation}
                />
            })}

            <Paginate jobsPerPage={postsPerPage}  totalJobs={jobs.length} paginate={paginate} currentPage={currentPage} changeLocation={changeLocation}/>
        </div>
    )
}
export default Jobs
