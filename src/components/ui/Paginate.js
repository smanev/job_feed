import React from 'react'

const Paginate = ({totalJobs, jobsPerPage, paginate, currentPage}) => {

    const pageNumbers = [];

    for (let i = 1; i < Math.ceil(totalJobs / jobsPerPage); i++) {
        pageNumbers.push(i)
    }
    
    
    
    
    return (
        <nav>
            <ul className='pagination'>
                {pageNumbers.map(number => (
                    <li key={number}>
                        <a role='button' onClick={(e) => paginate(e.target, number) }  className={`page-link ${number == currentPage && 'active'}`}>
                            {number}
                        </a>
                    </li>
                ))}
            </ul>
        </nav>
    )
}







export default Paginate
